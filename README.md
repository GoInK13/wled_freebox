# WLED_freebox

## Installation
    pip install freepybox
    git clone https://gitlab.com/GoInK13/wled_freebox.git
    cd wled_freebox

## Launch

	sudo nano /etc/systemd/system/wordClock.service
"""
[Unit]
Description=Word clock script to set brightness
After=default.target

[Service]
ExecStart=/home/pierrot/wordClock.py
User=pierrot

[Install]
WantedBy=default.target
"""

sudo systemctl enable wordClock.service
sudo systemctl daemon-reload
sudo systemctl start wordClock.service
sudo systemctl status wordClock.service
sudo systemctl stop wordClock.service
